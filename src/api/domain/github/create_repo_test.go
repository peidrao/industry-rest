package github

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateRepoRequestAsJson(t *testing.T) {
	request := CreateRepoRequest{
		Name:        "Golang Study",
		Description: "Studies",
		HomePage:    "https://www.github.com/peidrao/peidrao",
		Private:     true,
		HasIssues:   false,
		HasProject:  true,
		HasWiki:     true,
	}
	bytes, err := json.Marshal(request)
	assert.Nil(t, err)
	assert.NotNil(t, bytes)
	var target CreateRepoRequest
	err = json.Unmarshal(bytes, &target)
	assert.Nil(t, err)

	assert.Equal(t, target.Name, request.Name)
	assert.Equal(t, target.Description, request.Description)
	assert.Equal(t, target.HomePage, request.HomePage)
	assert.Equal(t, target.Private, request.Private)
	assert.Equal(t, target.HasIssues, request.HasIssues)
	assert.Equal(t, target.HasProject, request.HasProject)
	assert.Equal(t, target.HasWiki, request.HasWiki)
}
