package domain

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetUserNotUserFound(t *testing.T) {
	user, err := GetUser(0)

	assert.Nil(t, user, "we were not expecting a user with id 0")
	// if user != nil {
	// 	t.Error("we were not expecting a user with id 0")
	// }

	assert.NotNil(t, err, "we were expecting an error when user id is 0")
	// if err == nil {
	// 	t.Error("we were expecting an error when user id is 0")
	// }

	assert.EqualValues(t, http.StatusNotFound, err.StatusCode, "we were expecting an error when user id is 0")
	// if err.StatusCode != http.StatusNotFound {
	// 	t.Error("we were expecting 404 when user is not found")
	// }

	assert.EqualValues(t, "not_found", err.Code)
	assert.EqualValues(t, "user 0 was not found", err.Message)
}

func TestGetUser(t *testing.T) {
	user, err := GetUser(123)

	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.EqualValues(t, user.Id, 123)
	assert.EqualValues(t, user.FirstName, "Pedro")
	assert.EqualValues(t, user.LastName, "Fonseca")
	assert.EqualValues(t, user.Email, "contatopedrorn@gmail")
}
