package domain

import (
	"fmt"
	"log"

	"mvc/utils"
	"net/http"
)

var (
	users = map[int64]*User{
		123: {Id: 123, FirstName: "Pedro", LastName: "Fonseca", Email: "contatopedrorn@gmail"},
		124: {Id: 124, FirstName: "Maria", LastName: "Carolina", Email: "mscarolina@gmail"},
	}

	UserDAO userDAOInterface
)

func init() {
	UserDAO = &userDAO{}
}

type userDAOInterface interface {
	GetUser(int64) (*User, *utils.ApplicationError)
}

type userDAO struct{}

func (u *userDAO) GetUser(userId int64) (*User, *utils.ApplicationError) {
	log.Println("we're accessing the database")

	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message:    fmt.Sprintf("user %v was not found", userId),
		StatusCode: http.StatusNotFound,
		Code:       "not_found",
	}
}
