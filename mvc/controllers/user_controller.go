package controllers

import (
	"mvc/services"
	"mvc/utils"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetUser(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Param("user_id"), 10, 64)

	if err != nil {
		apiErr := &utils.ApplicationError{
			Message:    "user_id must be a number",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}

		utils.ResponseError(c, apiErr)
		return
	}

	user, apiErr := services.UsersServices.GetUser(userId)
	if apiErr != nil {
		utils.ResponseError(c, apiErr)
		return
	}

	utils.Response(c, http.StatusOK, user)
}
