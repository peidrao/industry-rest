package services

import (
	"mvc/domain"
	"mvc/utils"
)

type usersService struct {
}

var (
	UsersServices usersService
)

func (u *usersService) GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.UserDAO.GetUser(userId)
}
